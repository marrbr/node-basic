var express = require('express')
  , FeedParser = require('feedparser')
  , htmlParser = require('html-parser')
  , request = require('request')
  , tumblr = require('tumblr.js');

var app = express();
var ip = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';

var client = tumblr.createClient({
  consumer_key: 'BHu0IgZ582HfSi9rPLaByH6BAMeIfxWTNqUrgYVLGY8nwJYPrc',
  consumer_secret: 'tO9ceC701f0OLdOU0q4StzCuE555i6xeAVBvNRl7s6BBFXlMnJ',
  token: 'P1LxjlLq4B2jDkzXRtV4ZXrlOTbTsPwH7ViN2sH2T7dgkyu7Ct',
  token_secret: 'DYJV631zwXIw5oIZsI6wIpOV7tRVeNatiYyuuTyLOFGt5WTd2o'
});

var feedUrl = 'https://www.reddit.com/r/gonewild/top/.rss';
var processados = [
  't3_6v2dbt', 't3_6v6qup', 't3_6v5orc', 't3_6v5ksp', 't3_6v408g', 't3_6v6tht',
  't3_6v2v3y', 't3_6v2a0n', 't3_6v3erb', 't3_6v6e0i', 't3_6v4v11', 't3_6v51nf',
  't3_6v6qvi', 't3_6v20ch', 't3_6v28p8', 't3_6v3k4t', 't3_6v2sx7', 't3_6v2fpi',
  't3_6v4lil', 't3_6v7y4c', 't3_6v2k03', 't3_6v0uu6', 't3_6v7ksu', 't3_6v38hu',
  't3_6v3vr4', 't3_6v6t6y'
]

function photoPost(caption, link, source) {
  client.createPhotoPost(
    'macho-brasilia.tumblr.com',
    {
      state: 'queue',
      tags: 'brasília',
      caption: caption,
      link: link,
      source: source
    },
    function (err, data) {
        console.log(data);
    });
}

function fetch(feed) {
  console.log('iniciando ciclo');
  // Define our streams
  var req = request(feed, {timeout: 10000, pool: false});
  req.setMaxListeners(50);
  // Some feeds do not respond without user-agent and accept headers.
  req.setHeader('user-agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36');
  req.setHeader('accept', 'text/html,application/xhtml+xml');

  var feedparser = new FeedParser({addmeta: false});

  // Define our handlers
  req.on('error', done);
  req.on('response', function(res) {
    if (res.statusCode != 200) return this.emit('error', new Error('Bad status code'));
    res.pipe(feedparser);
  });

  feedparser.on('error', done);
  feedparser.on('end', done);
  feedparser.on('readable', function () {
    // This is where the action is!
    var stream = this; // `this` is `feedparser`, which is a stream
    var meta = this.meta; // **NOTE** the "meta" is always available in the context of the feedparser instance
    var item;

    while (item = stream.read()) {
      var id = item['atom:id']['#'];
      if (processados.indexOf(id) == -1){
        console.log(id);
        processarItem(item);
        processados.push(id);
      }
    }
  });
}

function done(err) {
  if (err) {
    console.log(err, err.stack);
    // return process.exit(1);
  }
  // process.exit();
}

function processarItem(item) {
  var caption = item.title;
  var link = item.link;

  var attributes = [];
  htmlParser.parse(item.description, {
    attribute: function(name, value) { attributes.push(value); },
  });
  var imgUrl = attributes[5]
  var address = imgUrl.split('/');
  if(address[2] == 'i.redd.it' || address[2] == 'i.imgur.com'){
    photoPost(caption, link, imgUrl)
  } else if (address[2] == 'imgur.com' || address[2] == 'm.imgur.com'){
    if (address[3].length == 7){
      var source = 'http://i.imgur.com/'+address[3]+'.jpg';
      photoPost(caption, link, source)
    } else if (address[3] == 'a'){
      console.log('É um album do imgur');
      console.log(imgUrl);
      console.log(address[4]);
    }
  } else if (address[2] == 'gfycat.com'){
      console.log('É um gif gfycat');
      console.log('https://thumbs.gfycat.com/'+address[3]+'-size_restricted.gif');
      console.log("<div style='position:relative;padding-bottom:178%'> " +
                    "<iframe src='https://gfycat.com/ifr/"+ address[3] +"' " +
                    "frameborder='0' scrolling='no' width='100%' height='100%' " +
                    "style='position:absolute;top:0;left:0;' allowfullscreen></iframe></div>");
      console.log(address[3]);
  } else {
    console.log('NÃO SEI O QUE É');
    console.log(imgUrl);
  }
  // console.log(item.description);
  console.log('');
}

fetch(feedUrl);
setInterval(function(){ fetch(feedUrl);}, 300000);

// app is running!
app.get('/', function(req, res) {
    res.send('NodeJS App rodando às '+ new Date());
});
app.listen(8080, ip);
